# AUTOGENERATED BY THE GRADLE SCRIPT. DO NOT EDIT.

require 'jar_dependencies'
require_jar('io.netty', 'netty-buffer', '4.1.87.Final')
require_jar('io.netty', 'netty-codec', '4.1.87.Final')
require_jar('io.netty', 'netty-codec-http', '4.1.87.Final')
require_jar('io.netty', 'netty-common', '4.1.87.Final')
require_jar('io.netty', 'netty-transport', '4.1.87.Final')
require_jar('io.netty', 'netty-handler', '4.1.87.Final')
require_jar('io.netty', 'netty-transport-native-unix-common', '4.1.87.Final')
require_jar('org.logstash.plugins.input.http', 'logstash-input-http', '3.6.1')
